/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class ColorAdapter(private val inflater: LayoutInflater) :
  ListAdapter<Int, ColorViewHolder>(ColorDiffer) {

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ColorViewHolder {
    return ColorViewHolder(inflater.inflate(R.layout.row, parent, false))
  }

  override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
    holder.bindTo(getItem(position))
  }

  private object ColorDiffer : DiffUtil.ItemCallback<Int>() {
    override fun areItemsTheSame(oldColor: Int, newColor: Int): Boolean {
      return oldColor == newColor
    }

    override fun areContentsTheSame(oldColor: Int, newColor: Int): Boolean {
      return areItemsTheSame(oldColor, newColor)
    }
  }
}
