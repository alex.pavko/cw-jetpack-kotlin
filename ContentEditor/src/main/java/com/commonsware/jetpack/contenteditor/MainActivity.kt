/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

private const val FILENAME = "test.txt"
private const val REQUEST_SAF = 1337
private const val REQUEST_PERMS = 123

class MainActivity : AppCompatActivity() {
  private lateinit var motor: MainMotor
  private var current: Uri? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val progress = findViewById<View>(R.id.progress)
    val title = findViewById<TextView>(R.id.title)

    motor = ViewModelProviders.of(this).get(MainMotor::class.java)

    motor.results.observe(this) { result ->
      when (result) {
        StreamResult.Loading -> {
          progress.visibility = View.VISIBLE
          text.isEnabled = false
        }
        is StreamResult.Content -> {
          progress.visibility = View.GONE
          text.isEnabled = true
          current = result.source
          title.text = result.source.toString()

          if (TextUtils.isEmpty(text.text)) {
            text.setText(result.text)
          }
        }
        is StreamResult.Error -> {
          progress.visibility = View.GONE
          text.setText(result.throwable.localizedMessage)
          text.isEnabled = false
          Log.e("ContentEditor", "Exception in I/O", result.throwable)
        }
      }
    }

    loadFromDir(filesDir)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.loadInternal -> {
        loadFromDir(filesDir)
        return true
      }

      R.id.loadExternal -> {
        loadFromDir(getExternalFilesDir(null))
        return true
      }

      R.id.loadExternalRoot -> {
        loadFromExternalRoot()
        return true
      }

      R.id.openDoc -> {
        startActivityForResult(
          Intent(Intent.ACTION_OPEN_DOCUMENT)
            .setType("text/*")
            .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF
        )
        return true
      }

      R.id.newDoc -> {
        startActivityForResult(
          Intent(Intent.ACTION_CREATE_DOCUMENT)
            .setType("text/plain")
            .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF
        )
        return true
      }

      R.id.save -> {
        current?.let { motor.write(it, text.text.toString()) }
        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }

  override fun onActivityResult(
    requestCode: Int, resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_SAF) {
      if (resultCode == Activity.RESULT_OK && data != null) {
        text.setText("")
        data.data?.let { motor.read(it) }
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<String>,
    grantResults: IntArray
  ) {
    if (requestCode == REQUEST_PERMS) {
      if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        loadFromDir(Environment.getExternalStorageDirectory())
      } else {
        Toast.makeText(this, R.string.msg_sorry, Toast.LENGTH_LONG).show()
      }
    }
  }

  private fun loadFromDir(dir: File?) {
    text.setText("")
    motor.read(Uri.fromFile(File(dir, FILENAME)))
  }

  private fun loadFromExternalRoot() {
    if (ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
      ) == PackageManager.PERMISSION_GRANTED
    ) {
      loadFromDir(Environment.getExternalStorageDirectory())
    } else {
      val perms = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

      ActivityCompat.requestPermissions(this, perms, REQUEST_PERMS)
    }
  }
}
