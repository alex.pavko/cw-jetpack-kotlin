/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
  private lateinit var motor: MainMotor
  private var wordCount = 6

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    motor = ViewModelProviders.of(this)[MainMotor::class.java]

    motor.results.observe(this) { viewState ->
      when (viewState) {
        MainViewState.Loading -> {
          progress.visibility = View.VISIBLE
          passphrase.text = ""
        }
        is MainViewState.Content -> {
          progress.visibility = View.GONE
          passphrase.text = viewState.passphrase
        }
        is MainViewState.Error -> {
          progress.visibility = View.GONE
          passphrase.text = viewState.throwable.localizedMessage
          Log.e(
            "Diceware",
            "Exception generating passphrase",
            viewState.throwable
          )
        }
      }
    }

    motor.generatePassphrase(wordCount)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.refresh -> {
        motor.generatePassphrase(wordCount)
        return true
      }

      R.id.word_count_4, R.id.word_count_5, R.id.word_count_6, R.id.word_count_7,
      R.id.word_count_8, R.id.word_count_9, R.id.word_count_10 -> {
        item.isChecked = !item.isChecked

        val temp = Integer.parseInt(item.title.toString())

        if (temp != wordCount) {
          wordCount = temp
          motor.generatePassphrase(wordCount)
        }

        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }
}
