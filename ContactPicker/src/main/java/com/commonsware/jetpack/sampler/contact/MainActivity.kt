/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.contact

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*

private const val REQUEST_PICK = 1337

class MainActivity : AppCompatActivity() {
  private lateinit var vm: ContactViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    vm = ViewModelProviders
      .of(this, ContactViewModelFactory(savedInstanceState))
      .get(ContactViewModel::class.java)
    updateViewButton()

    pick.setOnClickListener {
      try {
        startActivityForResult(
          Intent(
            Intent.ACTION_PICK,
            ContactsContract.Contacts.CONTENT_URI
          ), REQUEST_PICK
        )
      } catch (e: Exception) {
        Toast.makeText(this, R.string.msg_pick_error, Toast.LENGTH_LONG).show()
      }
    }

    view.setOnClickListener {
      try {
        startActivity(Intent(Intent.ACTION_VIEW, vm.contact))
      } catch (e: Exception) {
        Toast.makeText(this, R.string.msg_view_error, Toast.LENGTH_LONG).show()
      }
    }
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_PICK && resultCode == Activity.RESULT_OK &&
      data != null
    ) {
      vm.contact = data.data
      updateViewButton()
    }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    vm.onSaveInstanceState(outState)
  }

  private fun updateViewButton() {
    if (vm.contact != null) {
      view.isEnabled = true
    }
  }
}
