/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
  private lateinit var motor: MainMotor

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    motor = ViewModelProviders.of(this)[MainMotor::class.java]

    saveBookmark(intent)

    val adapter = BookmarkAdapter(layoutInflater)

    bookmarks.layoutManager = LinearLayoutManager(this)
    bookmarks.addItemDecoration(
      DividerItemDecoration(
        this,
        DividerItemDecoration.VERTICAL
      )
    )
    bookmarks.adapter = adapter

    motor.states.observe(this) { state ->
      when (state) {
        is MainViewState.Content -> adapter.submitList(state.rows)
        is MainViewState.Error -> {
          Toast.makeText(
            this@MainActivity, state.throwable.localizedMessage,
            Toast.LENGTH_LONG
          ).show()
          Log.e("Bookmarker", "Exception loading data", state.throwable)
        }
      }
    }

    motor.saveEvents.observe(this) { event ->
      event.handle { result ->
        val message = if (result.error == null) {
          result.content?.title + " was saved!"
        } else {
          result.error.localizedMessage
        }

        Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
      }
    }
  }

  private fun saveBookmark(intent: Intent) {
    if (Intent.ACTION_SEND == intent.action) {
      val pageUrl = getIntent().getStringExtra(Intent.EXTRA_STREAM)
        ?: getIntent().getStringExtra(Intent.EXTRA_TEXT)

      if (pageUrl != null && Uri.parse(pageUrl).scheme!!.startsWith("http")) {
        motor.save(pageUrl)
      } else {
        Toast.makeText(this, R.string.msg_invalid_url, Toast.LENGTH_LONG).show()
        finish()
      }
    }
  }
}
