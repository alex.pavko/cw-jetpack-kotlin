/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class MainMotor(application: Application) : AndroidViewModel(application) {
  private val _saveEvents = MutableLiveData<Event<BookmarkResult>>()
  val saveEvents: LiveData<Event<BookmarkResult>> = _saveEvents
  val states: LiveData<MainViewState>

  init {
    states =
      Transformations.map(BookmarkRepository.load(getApplication())) { models ->
        val content = ArrayList<RowState>()

        for (model in models) {
          content.add(RowState(model))
        }

        MainViewState.Content(content)
      }
  }

  fun save(pageUrl: String) {
    viewModelScope.launch(Dispatchers.Main) {
      _saveEvents.value = try {
        val model = BookmarkRepository.save(getApplication(), pageUrl)

        Event(BookmarkResult(model, null))
      } catch (t: Throwable) {
        Event(BookmarkResult(null, t))
      }
    }
  }
}
